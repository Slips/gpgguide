
# GPG For Dummies

## PREWORD
With the EARN IT act looming above americans, many have begun to worry about the security and privacy of their communications. This is where GPG may be helpful. GPG is an encryption utility developed by GNU as an extension and improved version of PGP. GPG allows you to achieve full End to End encryption over any service as long as it supports posting text. At first, it may be confusing, but this guide should make it simple. One more note: this guide is primarly for GNU/Linux users ~~as Windows is already backdoored. :P~~ \uj Most graphical GPG applications on Windows are generally self explanatory, however some of the concepts in this guide may prove useful to a beginner user.

## Part 1: Getting Started

To get started, you'll first need to generate a full gpg keypair. This can be accomplished with the `gpg --full-generate-key` option.

Fill out the required boxes and soon, you'll have your very own GPG Keypair. Here's what you need to know:

- This key is made up of two things: Your private key, and your public key.
- Your public key is free to share with anyone. It is used by other parties and can only be used to encrypt files, not decrypt them.
- Your private key is only to ever be shared with you. Ever. It is to be used only by yourself and can only be used to decrypt files, not encrypt them.

## Part 2: Sharing your key

To share your key is quite simple. Simply use this command:
```
gpg --export -a keyID
```

KeyID can be the email used by your keypair, or the public (not private) KeyID itself. This will post your GPG key into stdin with a format similar to this:
```
-----BEGIN PGP PUBLIC KEY BLOCK-----
(A large amount of jumbled text here)
-----END PGP PUBLIC KEY BLOCK-----
```
Of course, this just being pasted in your terminal isn't very useful, so look over it in your terminal and then run the command, this time piping the output to a file like this:
```
gpg --export -a keyID > MyPublicKey.gpg
```
This can be sent to any of your friends over text or shared as a file!

While we're here, we should probably go over what the `-a` flag means.

### Part 2.5: Ascii Armor

The `-a` flag is used by GPG to encase your encrypted messages and keys in Ascii Armor. The reason for this is that encrypted files/keys are usually exported in binary format, which, while previously useful with older PGP versions and fine for when you're just sharing files, is not really conducive to the concept of "End To End Encryption anywhere you can text". So the `-a` flag will convert it into an easily pastable ASCII character jumble. You should pretty much always have this flag set whenever making files (i.e exporting or encrypting) with GPG.

## Part 3: Importing other people's keys:

Of course, there's no reason to use GPG if you don't have anyone's keys to encrypt with, so you will need to import some. doing so is as easy as downloading the key, and importing it from the keyfile.
```
gpg --import /path/to/keyfile
```
Alternatively, you can have it accept keyfiles from stdin, such as from the output of your clipboard:
```
xsel -bo | gpg --import
```

## Part 4: Encryption
Now comes the fun part: encrypting files for others. This is as simple as creating a text file, adding the text you want, and then encrypting it:
```
echo "Hello Encrypted World!" > ScrtMsg"
gpg -sea -r recipientkeyID ScrtMsg
```
This will ask you for your password, after which a file named something along the lines of ScrtMsg.asc (or whatever you named your text file followed by .asc). Lets go over the anatomy of this command, as some of the abbreviations are not clear at first glance:

`-s` - Sign the message with your key. This acts like a signature on a real document, proving the file or message contained within is yours. This can be omitted, but is recommended.

`-e` - Encrypt the file.

`-a` - As previously mentioned, ascii-armors your file.

`-r recipientkeyID` - the r stands for recipient; states the recipient of your file and hence, who it is encrypted for.

Put all together, this encrypts the message in ASCII to your listed recipient and then signs it with your own key.

Worth noting if you're in a multi-user thread and need to encrypt to mulitple people, there's no need to repeat this whole process for each message. Simply add the `-r` flag again and set another recipient, and it will automagically be added to the file. Keep in mind, however, that when the file is being decrypted any decryptors will be able to see every key the file was encrypted with.


Alternatively, you can pipe output from other commands to GPG, allowing you to skip making a text file altogether.
```
echo "Hello Encrypted World!" | gpg -sea -r recipientkeyID
```
This will paste the encrypted message to stdin, which can then be passed to your clipboard via `xsel` or similar tools, or sent to other programs. Or, if you want to, you can just pipe the output directly to a file.

One more thing to note, this isn't just limited to text. You can encrypt anything: Music, Videos, even executables can be passed through GPG.
## Part 5: Decryption
Fun part number 2. To decrypt files encrypted to you, simply use `gpg -d`:
```
gpg -d encryptedFile
```
This will ask you for your password, after which it will show you the now decrypted message.


### Conclusion
Congratulations, you now have a basic understanding of how to use GPG. Now go out there and fight the system!

Want to check your skills? Make an issue on this repo with A: A link to your pubkey, and B: an encrypted message for me, and if it works you'll get a free virtual cookie!
